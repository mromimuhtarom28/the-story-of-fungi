﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletRedDestroy : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("redBullet"))
        {
            Destroy(collision.gameObject);
        }
    }
}
