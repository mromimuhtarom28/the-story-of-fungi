﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletYellowDestroy : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("yellowBullet"))
        {
            Destroy(collision.gameObject);
        }
    }
}
