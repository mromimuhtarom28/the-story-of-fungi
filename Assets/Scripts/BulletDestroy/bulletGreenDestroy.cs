﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletGreenDestroy : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("greenBullet"))
        {
            Destroy(collision.gameObject);
        }
    }
}
