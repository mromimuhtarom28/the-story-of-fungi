﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletBlueDestroy : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("blueBullet"))
        {
            Destroy(collision.gameObject);
        }
    }
}
