﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowEnemyDestroy : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("yellowEnemy"))
        {
            Destroy(collision.gameObject);
        }
    }
}
