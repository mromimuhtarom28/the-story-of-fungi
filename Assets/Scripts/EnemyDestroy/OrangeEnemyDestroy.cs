﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrangeEnemyDestroy : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("orangeEnemy"))
        {
            Destroy(collision.gameObject);
        }
    }
}
