﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueEnemyDestroy : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("blueEnemy"))
        {
            Destroy(collision.gameObject);
        }
    }
}
