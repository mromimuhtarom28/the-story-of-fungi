﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedEnemyDestroy : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("redEnemy"))
        {
            Destroy(collision.gameObject);
        }
    }
}
