﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController : MonoBehaviour {

    public GameObject enemy;
    
    float randX;
    Vector2 whereToSpawn;
    public float spawnRate = 0f;
    float nextSpawn = 0.9f;
	// Use this for initialization
	void Start () {
        
	}

    void OnTriggerEnter2D(Collider2D OtherCollider)
    {
        EnemySpawnController hit = OtherCollider.gameObject.GetComponent<EnemySpawnController>();
        if (hit != null)
        {
            Destroy(hit.gameObject);
            Destroy(gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            randX = Random.Range(5.16f, -1.1f);
            whereToSpawn = new Vector2(randX, transform.position.y);
            Instantiate(enemy, whereToSpawn, Quaternion.identity);
        }
	}
}
