﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour {
    public float speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        moveSide();
	}

    public void moveSide()
    {
        if (gameObject.transform.localPosition.x <= -665.8f)
        {
            //gameObject.transform.localPosition = new Vector3(-593.17f, -251.545f, 0f);
        }
        else
        {
            gameObject.transform.localPosition -= new Vector3(speed * Time.deltaTime, 0f, 0f);
        }
    }
}
