﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_controller : MonoBehaviour {
    protected Joystick joystick;
    protected Joybutton joybutton;
    protected Orangebulletbtn orangebulletbtn;
    protected Greenbulletbtn greenbulletbtn;
    protected Redbulletbtn redbulletbtn;
    protected Bluebulletbtn bluebulletbtn;
    protected Yellowbulletbtn yellowbulletbtn;

    public float timeBetweenShots = 0.3333f; //untuk shoot sekali
    private float timestamp;//

    protected bool jump;

    //public bool bullet = true;
    public bool isGround = false;
    public bool isJump = false;
    public GameObject LeftBullet, RightBulletOrange, RightBulletGreen, RightBulletRed, RightBulletYellow, RightBulletBlue;
    Transform firePos;
    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
        joybutton = FindObjectOfType<Joybutton>();
        orangebulletbtn = FindObjectOfType<Orangebulletbtn>();
        greenbulletbtn  = FindObjectOfType<Greenbulletbtn>();
        redbulletbtn = FindObjectOfType<Redbulletbtn>();
        bluebulletbtn = FindObjectOfType<Bluebulletbtn>();
        yellowbulletbtn = FindObjectOfType<Yellowbulletbtn>();
        firePos = transform.Find("firePos");
    }
    // Update is called once per frame
    void Update()
    {

        var rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.velocity = new Vector3(joystick.Horizontal * 5f,
                                         rigidbody.velocity.y,
                                         joystick.Vertical * 10f);

       if (!jump && joybutton.Pressed)
       {
          if (isGround == true)
          {
              jump = true;
              rigidbody.velocity += Vector2.up * 10f;
               isGround = false;
          }
            
        }
        
        if (jump && !joybutton.Pressed)
        {
            jump = false;
        }

        
            fire();
            
            
      

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("floor"))
        {
            isGround = true;
        }
       
        if (collision.gameObject.CompareTag("level1"))
        {
            //Application.LoadLevel("level2");
            SceneManager.LoadScene(2);
        }

        if (collision.gameObject.CompareTag("level2"))
        {
            SceneManager.LoadScene(3);
        }

        if (collision.gameObject.CompareTag("level3"))
        {
            SceneManager.LoadScene(4);
        }
    }

    void fire()
    {
        if ( Time.time >= timestamp && orangebulletbtn.Pressed)
        {        
                Instantiate(RightBulletOrange, firePos.position, Quaternion.identity);
                timestamp = Time.time + timeBetweenShots;
        }
        if (Time.time >= timestamp && greenbulletbtn.Pressed)
        {
                Instantiate(RightBulletGreen, firePos.position, Quaternion.identity);
                timestamp = Time.time + timeBetweenShots;
        }
        if (Time.time >= timestamp && redbulletbtn.Pressed)
        {
           
                Instantiate(RightBulletRed, firePos.position, Quaternion.identity);
                timestamp = Time.time + timeBetweenShots;
            
        }
        if (Time.time >= timestamp && bluebulletbtn.Pressed)
        {

            Instantiate(RightBulletBlue, firePos.position, Quaternion.identity);
            timestamp = Time.time + timeBetweenShots;

        }
        if (Time.time >= timestamp && yellowbulletbtn.Pressed)
        {

            Instantiate(RightBulletYellow, firePos.position, Quaternion.identity);
            timestamp = Time.time + timeBetweenShots;

        }
    }
}
