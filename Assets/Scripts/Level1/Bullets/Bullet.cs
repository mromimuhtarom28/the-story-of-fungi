﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float speed = 0.1f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Move();
	}

    public void Move()
    {
        gameObject.transform.position += new Vector3(0f, speed, 0f);
    }

    void OnBecameInvisible()
    {
        gameObject.SetActive(false);
    }
}
