﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPooler : MonoBehaviour {
    public GameObject pooledObject;
    public int pooledAmount;
    public bool isAllowGrow = false;
    public bool willGrow = false;
    public List<GameObject> pooledObjects;
	// Use this for initialization
	void Start () {
        SetPooledObject();
	}

    public void SetPooledObject()
    {
        //reset Array / List
        pooledObjects = new List<GameObject>();
        //Looping sebanyak pooledAmount
        for (int i = 0; i < pooledAmount; i++)
        {
            //Buat variable local yang diisi dengan object(Bullet) yang digenerated
            //dari prefabs
            GameObject obj = (GameObject)Instantiate(pooledObject);
            //Add object(Bullet) tersebut kedalam Array List
            pooledObjects.Add(obj);
        }
    }


    public GameObject GetPooledObject()
    {
        //Looping untuk mengecek object(Bullet) yang masih aktif di dalam hierarcy
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            //jika terdapat object yang disabled didalam hierarcy maka....
            if (!pooledObjects[i].activeInHierarchy)
            {
                //Mengembalikan object tersebut atau menggunakan object(bullet) tersebut
                //Sebagai object baru
                return pooledObjects[i];
            }
        }

        //Jika tidak ada object(Bullet) yang disabled dalam hierarcy maka
        //cek apakah pooler tersebut di izinkan untuk grow
        if (isAllowGrow)
        {
            //Jika dizinkan untuk grow maka
            if (willGrow)
            {
                //Buat sebuah variable local kemudian isi variable local tersebut
                //dengan generate objet(Bullet) dari prefabs
                GameObject obj = (GameObject)Instantiate(pooledObject);
                //Masukan object(Bullet) tersebut kedalam list
                pooledObjects.Add(obj);
                //Mengembalikan object tersebut atau menggunakan object tersebut
                //Sebagai object baru
                return obj;
            }
        }

        //Selamanya tidak akan dijalankan, jika sampai dijalankan maka berarti
        //sistem pooler bermasalah
        return null;
    }
}
